These are the sources for the annobin project:

  https://sourceware.org/annobin/

The projects consists of two main parts:

  1. A security checker program - annocheck - which examines binaries
     and reports potential security problems.

  2. A set of plugins for GCC, Clang and LLVM that records security
     information for use by annocheck.

In addition there is a testsuite and documentation too.

The project does not have its own Code of Conduct, but the guidelines
found in the Binutils Code of Conduct should be considered to apply
to this project as well:

  https://sourceware.org/binutils/code-of-conduct/Code-of-Conduct.html

